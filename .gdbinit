set confirm off
set verbose off
set history filename ~/.gdbhistory
set history save

#set output-radix 0x10
#set input-radix 0x10

# These make gdb never pause in its output
set height 0
set width 0
set extended-prompt \[\e[31m\]gdb\[\e[0m\]:\[\e[32m\]\f\[\e[0;1m\]$\[\e[0m\] 
set disassembly-flavor intel
