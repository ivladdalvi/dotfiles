case `uname` in
  "Darwin" )
    NETSTATOPTS="-an -f inet"
    PSOPTS="-e -o pid,user,cpu,rss,state,command"
    LSOPTS="-GFh"
    export LSCOLORS="Gxfxcxdxbxegedabagacad"
    if [ -x /Applications/MacVim.app/Contents/MacOS/Vim ]
    then
      EDITOR='/Applications/MacVim.app/Contents/MacOS/Vim -gf -c "au VimLeave * !open -a Terminal"'
    else
      EDITOR=$(which vim)
    fi
    alias airport="/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport"
    alias d="df -Hl"
    ;;

  "Linux" )
    NETSTATOPTS="-aptun"
    PSOPTS="-o pid,user,state,pcpu,vsize:8,args"
    LSOPTS="--time-style=long-iso --color=auto -Fh --group-directories-first"
    if [ -x $(which vim) ]
    then
      EDITOR=$(which vim)
    else
      EDITOR=$(which vi)
    fi
    alias d="df -x tmpfs -x devtmpfs -h"
    ;;

  "FreeBSD" )
    PSOPTS="-axww -o pid,user,pcpu,rss,state,args"
    LSOPTS="-GFh"
    NETSTATOPTS="-an -f inet"
    if [ -x $(which vim) ]
    then
      EDITOR=$(which vim)
    else
      EDITOR=$(which vi)
    fi
    alias d="df -hl"
    ;;

  * )
    NETSTATOPTS="-aptun"
    PSOPTS="-o pid,user,pcpu,size,args"
    LSOPTS="-F"
    if [ -x $(which vim) ]
    then
      EDITOR=$(which vim)
    else
      EDITOR=$(which vi)
    fi
    alias d="df -hl"
    ;;
esac

n () {
  if [ ! -z $1 ]
  then
    sudo netstat `echo ${NETSTATOPTS}`|awk -v pattern="$1" '(NR == 1 || NR == 2 || $0 ~ pattern) { print }'
    #sudo netstat `echo ${NETSTATOPTS}`|awk -v pattern="$1" '(NR == 1 || NR == 2 || $0 ~ pattern) { print }'|grcat conf.netstat
  else
    sudo netstat `echo ${NETSTATOPTS}`
    #sudo netstat `echo ${NETSTATOPTS}`|grcat conf.netstat
  fi
}
#unset NETSTATOPTS

p () {
  if [ ! -z $1 ]; then
    #ps `echo ${PSOPTS}` | awk -v pattern="$1" '(NR == 1 || tolower($0) ~ tolower(pattern)) {print}'
    ps $(echo "${PSOPTS}") --pid $(pgrep $1)
  else
    ps --pid 2 --ppid 2 --deselect -H $(echo "${PSOPTS}")
  fi
}
#unset PSOPTS

ff () {
  if [ "$#" -eq 0 ];
  then
    tabs 20,32
    find . -name ".git" -prune -o -type f,l -printf "%M %u\t%s\t%TF %p\n"
    tabs
  else
    tabs 20,32
    find "$1" -name ".git" -prune -o -type f,l -printf "%M %u\t%s\t%TF %p\n"
    tabs
  fi
}


export EDITOR
if [ ! -z ${EDITOR} ] # is vim or vi
then
  alias v=${EDITOR}
fi

if [ ! -z ${PAGER} ] # is less or more
then
  alias m=${PAGER}
fi

# use lesspipe for advanced viewing
if [ -x /opt/local/bin/lesspipe.sh ]
then
  export LESSOPEN='| /opt/local/bin/lesspipe.sh %s'
fi

alias l="ls ${LSOPTS}"
alias ll="ls -l ${LSOPTS}"
alias la="ls -A ${LSOPTS}"
alias lla="ls -lA ${LSOPTS}"

alias h=history
alias g="egrep -i --color=auto"
alias s="sudo"
alias ssto="ssh-add -l|grep -qF librtpkcs11ecp.dylib && ssh-add -e /usr/local/lib/librtpkcs11ecp.dylib; ssh-add -s /usr/local/lib/librtpkcs11ecp.dylib"
alias speed='curl -L --silent -o /dev/null -w "%{url_effective} (%{remote_ip}): %{http_code}/%{num_redirects} , Connect: %{time_connect}s, Lookup: %{time_namelookup}s, Total: %{time_total}s\n"'

bindkey -v               # vi key bindings
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey -M vicmd "k" up-line-or-beginning-search
bindkey -M vicmd "j" down-line-or-beginning-search
bindkey "^P" history-beginning-search-backward
bindkey "^N" history-beginning-search-forward
bindkey ' '  magic-space # complete !-substitutions on space

# "v" to edit current line in $EDITOR
autoload -U edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

umask 077

setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_no_store
setopt hist_reduce_blanks
unsetopt nomatch

preexec () {
  # set screen title
  if [ "$TERM" == "screen" ]; then
    echo -ne "\ek${1[(w)1]}\e\\"
  fi

# Save data for the check in precmd()
CMD_START_DATE=$(print -P %D{%s})
CMD_NAME=${1[(w)1]}
}

precmd () {
  # set screen title
  if [ "$TERM" == "screen" ]; then
    echo -ne "\ekzsh\e\\"
  fi

# Beep if the program was running long time
# Proceed only if we've ran a command in the current shell.
if ! [[ -z $CMD_START_DATE ]]; then
  CMD_END_DATE=$(print -P %D{%s})
  CMD_ELAPSED_TIME=$(($CMD_END_DATE - $CMD_START_DATE))
  CMD_NOTIFY_THRESHOLD=30
  # Do not beep on some commands
  CMD_WHITE_LIST=(vim ssh screen python3 man)
  if [[ ${CMD_WHITE_LIST[(i)$CMD_NAME]} -gt ${#CMD_WHITE_LIST} ]]; then
    if [[ $CMD_ELAPSED_TIME -gt $CMD_NOTIFY_THRESHOLD ]]; then
      print -n '\a'
      #notify-send 'Job finished' "The job \"$CMD_NAME\" has finished."
    fi
  fi
fi
}

if [ -n "${SSH_CONNECTION}" ]; then
  export PS1='%(?..[%F{red}%B%?%f%b] )%F{green}%m %B%F{yellow}%32<..<%~ %(#.%F{red}.%F{cyan})❯%b%f '
else
  export PS1='%(?..[%F{red}%B%?%f%b] )%B%F{yellow}%32<..<%~ %(#.%F{red}.%F{cyan})❯%b%f '
fi

# Shell functions
setenv() { typeset -x "${1}${1:+=}${(@)argv[2,$#]}" }  # csh compatibility
freload() { while (( $# )); do; unfunction $1; autoload -U $1; shift; done }

# Where to look for autoloaded function definitions
fpath=($fpath ~/.zfunc)

# Autoload all shell functions from all directories in $fpath (following
# symlinks) that have the executable bit on (the executable bit is not
# necessary, but gives you an easy way to stop the autoloading of a
# particular shell function). $fpath should not be empty for this to work.
for func in $^fpath/*(N-.x:t); autoload $func

# automatically remove duplicates from these arrays
typeset -U path cdpath fpath manpath

# Hosts to use for completion (see later zstyle)
hosts=(${$(hostname):l} tsadi.nxadi.com)

# Set prompts
#PROMPT='%m%# '    # default prompt
#RPROMPT=' %~'     # prompt for right side of screen

# Some environment variables
#export MAIL=/var/spool/mail/$USERNAME
export LESS="-cex3MXRF~"
export LESS_TERMCAP_md=$'\E[01;37m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[33m'

export HELPDIR=/usr/local/lib/zsh/help  # directory for run-help function to find docs
export BC_ENV_ARGS="-ql"

MAILCHECK=300
HISTSIZE=1000
export HISTFILE="${HOME}/.zhistory"
SAVEHIST=1000
DIRSTACKSIZE=20

# Watch for my friends
#watch=( $(<~/.friends) )       # watch for people in .friends file
watch=(notme)                   # watch for everybody but me
LOGCHECK=300                    # check every 5 min for login/logout activity
WATCHFMT='%n %a %l from %m at %t.'

# Set/unset  shell options
setopt   notify globdots pushdtohome cdablevars autolist
setopt   autocd recexact longlistjobs
setopt   autoresume histignoredups pushdsilent clobber
setopt   autopushd pushdminus extendedglob rcquotes mailwarning
setopt pushdignoredups
unsetopt bgnice autoparamslash
setopt nocorrect nocorrectall

# Autoload zsh modules when they are referenced
zmodload -a zsh/stat stat
zmodload -a zsh/zpty zpty
zmodload -a zsh/zprof zprof
zmodload -ap zsh/mapfile mapfile


# Setup new style completion system. To see examples of the old style (compctl
# based) programmable completion, check Misc/compctl-examples in the zsh
# distribution.
autoload -U compinit
compinit

# Completion Styles

# list of completers to use
zstyle ':completion:*::::' completer _expand _complete _ignored #_approximate

# allow one error for every three characters typed in approximate completer
#zstyle -e ':completion:*:approximate:*' max-errors \
#    'reply=( $(( ($#PREFIX+$#SUFFIX)/3 )) numeric )'

# insert all expansions for expand completer
zstyle ':completion:*:expand:*' tag-order all-expansions

# formatting and messages
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
#zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''

# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# command for process lists, the local web server details and host completion
#zstyle ':completion:*:processes' command 'ps -o pid,s,nice,stime,args'
#zstyle ':completion:*:urls' local 'www' '/var/www/htdocs' 'public_html'
zstyle '*' hosts $hosts

# Filename suffixes to ignore during completion (except after rm command)
zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.o' '*?.c~' \
  '*?.old' '*?.pro'
  # the same for old style completion
  #fignore=(.o .c~ .old .pro)

# ignore completion functions (until the _ignored completer)
zstyle ':completion:*:functions' ignored-patterns '_*'


# ssh completion
if [ -r ${HOME}/.ssh/known_hosts ]
then
  local knownhosts
  knownhosts=( ${${${${(f)"$(<$HOME/.ssh/known_hosts)"}:#[0-9]*}%%\ *}%%,*} ) 
fi
hosts=($hosts $knownhosts)
zstyle ':completion:*:(ssh|scp|sftp):*' hosts $hosts

compdef gpg2=gpg
compdef dofi=git

if [ $commands[kubectl] ]; then
  source <(kubectl completion zsh)
fi

if [ -x "${HOME}/testssl.sh/testssl.sh" ]; then
  alias ts='~/testssl.sh/testssl.sh --quiet --wide --color 3 --protocols --preference'
fi
